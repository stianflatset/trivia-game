import Vue from 'vue'
import VueRouter from 'vue-router'
import StartScreen from './components/StartScreen.vue'
import QuestionScreen from './components/QuestionScreen.vue'
import ResultScreen from './components/ResultScreen.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/start',
        alias: '/',
        component: StartScreen
    },
    {
        path: '/question',
        component: QuestionScreen
    },
    {
        path: '/result',
        component: ResultScreen
    }

]

export default new VueRouter({ routes })